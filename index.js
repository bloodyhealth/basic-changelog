#!/usr/bin/env node
'use strict'

const exec = require('child_process').exec
const unified = require('unified')
const parser = require('remark-parse')
const stringify = require('remark-stringify')
const vfile = require('to-vfile')
const gitTags = require('git-tags')
const u = require('unist-builder')
const multicb = require('multicb')

module.exports = (changelogPath, opts, cb) => {
  if (typeof opts === 'function') {
    cb = opts
    opts = {
      filterCommitsStartingWith: []
    }
  }
  unified()
    .use(parser, {commonmark: true})
    .use(updateChangelog)
    .use(stringify, {listItemIndent: '1'})
    .process(vfile.readSync(changelogPath), (err, file) => {
      if (err) return cb(err)
      vfile.writeSync(file)
      cb(null)
    })

  function updateChangelog() {
    const done = multicb()
    return (ast, _, doneModifyingAst) => {
      gitTags.get((err, tags) => {
        if (err) return doneModifyingAst(err)
        const headingsInChangeLog = ast.children.filter(isHeadingLevelTwo)
        tags.forEach((tag, i) => {
          const isAlreadyInChangelog = headingsInChangeLog.some(heading => {
            return heading.children[0].value.includes(tag)
          })
          if (isAlreadyInChangelog) return

          let command
          if (i === tags.length - 1) {
            command = `git log --no-merges --pretty=format:'%s' ${tag}`
          } else {
            command = `git log --no-merges --pretty=format:'%s' ${tags[i + 1]}...${tag}`
          }
          const doneExecutingCommand = done()
          exec(command, (err, stdout) => {
            doneExecutingCommand(err, stdout, tag)
          })
        })

        done((err, results) => {
          if (err) return doneModifyingAst(err)
          results.forEach(([err, gitLog, tag]) => {
            if (err) return doneModifyingAst(err)
            const listFormatted = gitLog
              .split('\n')
              .filter(commitText => {
                return !opts.filterCommitsStartingWith.some(string => commitText.startsWith(string))
              })
              .map(commitText => u('listItem', [u('text', commitText)]))

            const posBeforeNextHeading = ast.children.findIndex(isHeadingLevelTwo)
            ast.children.splice(posBeforeNextHeading, 0,
              u('heading', {depth: 2}, [u('text', tag)]),
              u('list', { ordered: false }, listFormatted)
            )
          })
          doneModifyingAst(null, ast)
        })
      })
    }
  }

  function isHeadingLevelTwo(node) {
    return node.type === 'heading' && node.depth === 2
  }
}
